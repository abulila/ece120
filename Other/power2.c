#include <stdio.h>
#include <stdbool.h>

bool isPowerOfTwo(unsigned int);

int main() {
	int n;
	printf("Enter a positive integer: ");
	scanf("%d", &n);

	if (isPowerOfTwo(n)) {
		printf("%d is a power of 2.", n);
	} else {
		printf("%d is not a power of 2.", n);
	}
	return 0;
}

bool isPowerOfTwo(unsigned int x) {
	return (x & (~x + 1)) == x;
}
