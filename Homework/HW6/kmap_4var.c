#include <stdio.h>
int main()
{
    unsigned int w, x, y, z, a, b;
    unsigned int f,g;

    /* Loop twice to print both functions */
    for (char func = 'f'; func <= 'g'; func ++) {

        /* Print header for K-map. */
        printf("              yz      \n");
        printf("  %c       00 01 11 10 \n", func);
        printf("         _____________\n");

        /* row-printing loop */
        for (w = 0; 2 > w; w ++) {

            /* Loop over a in binary order */
            for (a = 0; 2 > a; a ++) {

                /* use variables w and a to calculate *
                 * input variable x (iterated in      *
                 * Gray code order).                  */
                x = (a ^ w) & 1;

                /* print the variable names on the    *
                 * second row only                    */
                if (w == 0 && x == 1) {
                    printf("wx   %u%u  | ", w, x);
                } else {
                    printf("     %u%u  | ", w, x);
                }

                /* Loop over input variable y in binary order. */
                for (y = 0; 2 > y; y ++) {

                    /* Loop over b in binary order.*/
                    for (b = 0; 2 > b; b ++) {

                        /* Use variables y and b to calculate   *
                         * input variable z (iterated in        *
                         * Gray code order).                    */
                        z = (b ^ y) & 1;

                        /* Calculate and print one K-map entry  *
                         * (functions F(w,x,y,z), G(w,x,y,z) ). */
                        if (func == 'f') {
                            f = ((x & ~y) | (~w & z)) & 1;
                            printf("%u  ", f);
                        } else {
                            g = ((~w & x & y & ~z) | w | ~x) & 1;
                            printf("%u  ", g);
                        }
                    }
                }

                /* End of row reached: print a newline character. */
                printf("\n");
            }
        }
        /* End of a k-map, add a space before the next one */
        printf("\n");
    }
    return 0;
}
