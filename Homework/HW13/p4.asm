                .ORIG x3000
                LDI     R1, ADDRESS
                ; #-20 is too big to subtract in one step
                ; This is found in the first pass
                ADD     R1, R1, #-10
                ADD     R1, R1, #-10
                BRnp    FINISH
                LEA     R0, MESSAGE
                PUTS
FINISH          HALT
MESSAGE         .STRINGZ    "M[x3025] is equal to twenty"
ADDRESS         .FILL   x3025
                .END
