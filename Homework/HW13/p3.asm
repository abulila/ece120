                .ORIG x3000
                AND     R0, R0, #0
                ADD     R1, R0, #1
                ADD     R0, R0, #5
SHIFT           BRz     DONE
                ; R1 must be shifted before the counter
                ; is updated or the condition codes will be wrong.
                ADD     R1, R1, R1
                ADD     R0, R0, #-1
                BR      SHIFT
DONE            TRAP    x25
                .END
