#include <stdio.h>

int main () {
	int i;
	int sample;
	int average = 0;

	for (i = 1; 10 >= i; i = i + 1) {
		/* Read a sample. */
		printf("Enter sample #%d: ", i);
		if (1 != scanf("%d", &sample)) {
			printf("Numeric samples only!\n");
			return 3;
		}

		/* Process the next sample. */
		average = average + (sample / 10);
	}

	/* Print the results. */
	printf("The average is %d.\n", average);

	return 0;
}
